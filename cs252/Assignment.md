# Assingments for CS252: July-Dec 2016.


## Assignment 1. (Aug 6, 2016)

All these assignments have to be demonstrated in front of the TA. You
will be asked questions on it.

1. Show the network parameters like IP address, netmask, default
   gateway etc of your cloud container. You need to show these
   parameters on the shell command line using appropriate shell
   commands. It is *not enough* to show it on you admin panel.

2. Find out all networking services are running on your system that
   uses some tcp or udp port.

3. Find the network route, i.e. the intermediate nodes in the network,
   that is taken by a packet sent from here to www.iitb.ac.in.

4. Block access to ssh on your machine form vyom.cc.iitk.ac.in


# Assignment 2. (Aug 16, 2016)

1. Create a internal lan inside the cloud with the following configuration

   * LAN A contains machine X and Y
   * LAN B contains machine Z
   * Select two distinct address space of the LANS.
   * Connect the two LANs with a router.

2. After logging in to X (via the web console), get the hardware address of Y and the router.

3. Find out what arping does. From the machine X, which of Y and Z
   will be accessible via `arping` and why ?

# Assignment 3. (Aug 23, 2016).

For this assignment you would need to first build a network with the
following configuration.

* A machine that has two interfaces -  one connected to the outside
  network (i.e. in our case the CSE network) and one to an internal
  LAN. This will be our `gateway`.

* Two machines inside the LAN, called `heaven` and `hell`

* The LAN has address space `192.168.1.0/24`

Your task is to write firewall rules (using `iptables`) at the
`gateway` to achieve the following.

1. Setup the machine `gateway` that forwards all packets from the
   internal network to the outside world. You should perform a natting
   at `gateway` for this to work. As a result,  for all machines
   outside the network, it will appear that all packets are originating
   at the gateway.

2. An ssh connection to the port 2222 of `gateway` should actually
   connect to the ssh server running at its default port (22) at
   `hell`.

3. As the admin of the gateway you should setup the firewall rules as follows.

	* ssh access _from_ `hell` and `heaven` _to_ the outside world
      should be provided. You should be able to login to `turing.cse` from both these
	  machines.

	* Http access to outside world should be available from `hell` and
      not from `heaven`. For example, http connections from `hell` to
      `cse.iitk.ac.in` should work but not from `heaven`.
