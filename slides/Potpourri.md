% Potpourri
% Piyush P Kurur
% April 13, 2015

# Managing config files

## The Dot files.

- Unix programs configurations are stored in readable files

- How can we manage it in version control.

## The idea of a dotfiles repository

- Create a git/hg/darcs repository called dotfiles in your home

- Store the config files there *without* the dot

- A script to *install* the dotfiles by symlinking.

# Building a web application

## What is the problem?

- HTTP is state less, hence use cookies

- You can't trust the user ('s browser)

- You can't trust one user to not harm another.

## MVC approach

- Model, View, Controller

- Ruby on rails, Django, Yesod


## Framework takes care of:

- Interfacing models to DB.

- Views to html (with additional stuff)

- Controllers to urls.

## Advantage

- Thin on the client side

- Works with any browser.

- Everything on server side (use whatever platform you like)

## Disadvantage

- Models and DB have mismatch

- Feedback to users have to come from servers.


## Rich applications

- Server is really basic

- Everything happens at client side

- Communicates to server.

## Advantage

- More responsive

- Immediate Feedback

## Disadvantage

- Javascript

## "Solutions" to the Javascript problem

- Elm

- Purescript

- Disclaimer never tried.
